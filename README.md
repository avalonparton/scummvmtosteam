# ScummVMtoSteam

Script for adding ScummVM games to Steam.

# Screenshots

![screenshot](https://i.imgur.com/FAGT4va.png)

# Usage

1. Download and install [ScummVM](https://www.scummvm.org/downloads/) to the default directory.

1. Download the Datafiles needed for the games you want from [archive.org](https://archive.org/search.php?query=creator%3A%22Humongous+Entertainment%22)

1. Place the datafiles in the appropriate folders in `games/`

1. Delete any game folders you don't want (empty folders will cause shortcuts that don't work)

1. Install [steamclient](https://gitlab.com/avalonparton/steam-client)
   ```bash
   pip install steamclient
   ```

1. Run the script with [Python 3.8+](https://www.python.org/downloads/)

```bash
python import.py games
```
